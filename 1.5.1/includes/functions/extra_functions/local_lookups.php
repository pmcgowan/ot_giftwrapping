<?php
  // Modelled after zen_has_product_attributes_downloads_status().
  // Note that logic is inverted and an extra parm is added.
  function product_attributes_downloads_status($products_id, $attrlist) {
    global $db;
    if (DOWNLOAD_ENABLED == 'true') {
      $download_display_query_raw ="select pa.products_attributes_id, pa.options_id, pa.options_values_id  
                                    from " . TABLE_PRODUCTS_ATTRIBUTES . " pa, " . TABLE_PRODUCTS_ATTRIBUTES_DOWNLOAD . " pad
                                    where pa.products_id='" . $products_id . "'
                                      and pad.products_attributes_id= pa.products_attributes_id";

      $download_display = $db->Execute($download_display_query_raw);
      if ($download_display->RecordCount() == 0) {
        $valid_downloads = false;
      } else {
        $valid_downloads = false;
        while (!$download_display->EOF) {
           for ($i = 0; $i < sizeof($attrlist); $i++) {
               if ( ($download_display->fields['options_id'] == $attrlist[$i]['option_id']) && 
                    ($download_display->fields['options_values_id'] == $attrlist[$i]['value_id']) ) {
                  $valid_downloads = true;
                  break; 
              }
           }
           if (!$valid_downloads) 
              $download_display->MoveNext(); 
           else 
              break;
        }
      }
    } else {
      $valid_downloads = false;
    }
    return $valid_downloads;
  }
?>
