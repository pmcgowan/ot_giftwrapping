<?php
/**
 * giftwrap_tables.php
 * Defines giftwrap table.
 *
 * @package initSystem
 * @copyright Copyright 2003-2005 Zen Cart Development Team
 * @copyright Portions Copyright 2003 osCommerce
 * @license http://www.zen-cart.com/license/2_0.txt GNU Public License V2.0
 */

if (!defined('DB_PREFIX')) define('DB_PREFIX', '');
define('TABLE_ORDERS_GIFTWRAP', DB_PREFIX . 'orders_giftwrap');
?>
