<?php
//
// +----------------------------------------------------------------------+
// |zen-cart Open Source E-commerce                                       |
// +----------------------------------------------------------------------+
// | Copyright (c) 2006 That Software Guy                                 |
// | http://www.thatsoftwareguy.com                                       |
// |                                                                      |
// | Portions Copyright (c) 2003 osCommerce                               |
// | Portions Copyright (c) 2006 The Zen Cart Developers                  |
// +----------------------------------------------------------------------+
// | This source file is subject to version 2.0 of the GPL license,       |
// | that is bundled with this package in the file LICENSE, and is        |
// | available through the world-wide-web at the following url:           |
// | http://www.zen-cart.com/license/2_0.txt.                             |
// | If you did not receive a copy of the zen-cart license and are unable |
// | to obtain it through the world-wide-web, please send a note to       |
// | license@zen-cart.com so we can mail you a copy immediately.          |
// +----------------------------------------------------------------------+
//

  define('MODULE_ORDER_TOTAL_GIFTWRAP_CHECKOUT_TITLE', 'Gift Wrap');
  define('MODULE_ORDER_TOTAL_GIFTWRAP_CHECKOUT_DESCRIPTION', 'Gift Wrap');
?>
