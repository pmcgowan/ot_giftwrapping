Gift Wrapping for Zen Cart 1.5.0 and 1.5.1 
Version 2.9_1.5.x 
UPDATED FOR 1.5.x USE ONLY!!
Author: Scott Wilson
http://www.thatsoftwareguy.com 
Zen Forum PM swguy
--------------------------------------------------
Released under the GNU General Public License
See license.txt file.
--------------------------------------------------
No warranties expressed or implied; use at your own risk.

More help than you can even imagine is provided in 
www.thatsoftwareguy.com/zencart_giftwrap_checkout.html

History: 
2.9_1.5.x  09/18/2012 - Update to 1.5.1 
2.8_150_rc1 11/06/2011 - Update to 1.5.0 rc1 
2.8_150 07/26/2011 - Update to Beta 1.5.0 Release.
2.8_139 08/20/2010 - Fixed pricing issue
2.7_139 07/01/2010 - Added wrap selections
2.6_138 12/03/2008 - Added wrapping info to packing slip
2.5_138 07/23/2008 - Fixed issue with PayPal IPN
2.4_138 01/09/2008 - Fixed include tax issue
2.3_138 01/03/2008 - Updated for 1.3.8
2.3 12/18/2008 - Fixed an error in the updating of the order total table.
2.2 12/01/2007 - Added include_tax logic for VAT countries; aligned checkbox 
                 vertically, removed test code.
2.1 11/10/2007 - Added features: user exits for not offering wrapping on 
                 products and categories, user exits for surcharging wrapping,
                 ability to turn off user checkout wrapping from admin.
2.0 10/12/2007 - Rewritten so that gift wrapping can be selected on the 
                 shipping page.
1.0 12/11/2006 - First Release 
--------------------------------------------------
Overview: 
My first gift wrapping module required the use of attributes on each wrappable
product.  Clearly this was a barrier to adoption.  So in this version, I'm 
making wrapping selectable on the shipping page without attributes.  

--------------------------------------------------

Installation Instructions: 
0. Back up everything!  Try this in a test environment prior to installing
it on a live shop.

1. Unzip the file you have received.  Delete the file "refresh" if it exists.

2. Copy the contents of either 1.5.0 or 1.5.1 to 
the root directory of your shop.  NOTE that these files assume
your template name is "custom," so any file with "custom" 
in its name will need to be renamed if you are using a 
different template name.  

3a. Only perform this step if you have never installed Gift Wrap 2.0 or
greater:
   Run the sql script orders_wrap.sql in your admin panel under 
   Admin->Tools->Install SQL Patches. 

3b. Only perform this step if you have never installed Gift Wrap 2.7 or
greater:
   Run the sql script wrap_select.sql in your admin panel under 
   Admin->Tools->Install SQL Patches. 

4. Update the define GIFT_WRAP_EXPLAIN_DETAILS in the file 
./includes/languages/english/extra_definitions/giftwrap.php
to explain your policies.

5. Go to Admin->Modules->Order Total->Gift Wrap and press the Install button.

NOTE: If you are going from an older Gift Wrap module to a newer version, 
you MUST do a "Remove" and then an "Install" since database items for this 
module may have changed.

6. If you have installed a version of this module prior to version 2.7,
please delete includes/templates/YOUR_TEMPLATE/css/stylesheet_giftwrap.css.

7. Wrapping Selection: By default, the selection mechanism is a checkbox
next to every item on the checkout shipping page. 
Two other options are provided in the Wrap Selection Option 
(under Admin->Modules->Order Total->Gift Wrapping at Checkout): 

If you wish to use image swatches of your wrapping paper, add these 
images to images/giftwrap, and set the configuration value 
Wrap Selection Option to "Images".

If you wish to use text descriptions of your wrapping paper, add these 
descriptions to the $wrap_selections array in  
./includes/languages/english/extra_definitions/giftwrap.php 
and set the configuration value 
Wrap Selection Option to "Descriptions".

Optional Customizations: 

1. If you are running Super Orders, edit the file admin/super_orders.php
and find the following line:

  require(DIR_WS_MODULES . 'orders_download.php');

Now go to just before this line; this is where you want to paste the code.
The code you want is in orders.php between

<!-- bof gift wrap -->
and 
<!-- eof gift wrap -->


2. Similarly, for Super Orders users, edit admin/super_packingslip.php 
and find the following line: 

    <?php if ($customer_notes) { ?>

Now go to just before this line; this is where you want to paste the code.
The code you want is in packingslip.php between

<!-- bof gift wrap -->
and 
<!-- eof gift wrap -->

 
New Files
=========
./includes/languages/english/extra_definitions/giftwrap.php
./includes/languages/english/modules/order_total/ot_giftwrap_checkout.php
./includes/modules/order_total/ot_giftwrap_checkout.php
./includes/extra_datafiles/giftwrap_tables.php
./includes/functions/extra_functions/local_lookups.php
./includes/extra_configures/giftwrap_settings.php
./admin/includes/languages/english/extra_definitions/giftwrap.php
./admin/includes/extra_configures/giftwrap_tables.php
./admin/includes/extra_configures/giftwrap_settings.php

Modified Files (No Overrides)
=============================
./includes/classes/order.php
./includes/modules/pages/checkout_shipping/header_php.php
./admin/invoice.php
./admin/orders.php
./admin/packingslip.php
./admin/includes/classes/order.php

Overrides
=========
./includes/templates/custom/templates/tpl_checkout_shipping_default.php
./includes/templates/custom/templates/tpl_account_history_info_default.php
./includes/templates/custom/templates/tpl_checkout_confirmation_default.php

